import { loginApi, getInfoApi, loginOutApi } from '@/api/user'
import { ActionContext } from 'vuex'
import { ElMessage } from 'element-plus'

interface State {
  token: string,
  info: object,
  menu: object,
  orgList: object,
  authoritys: object
}
const state = (): State => ({
  token: '', // 登录token
  info: {},  // 用户信息
  menu: [],  // 目录信息
  orgList: [], //组织列表
  authoritys: [], //权限列表
})

// getters
const getters = {
  token(state: State) {
    return state.token
  }
}

// mutations
const mutations = {
  tokenChange(state: State, token: string) {
    state.token = token
  },
  infoChange(state: State, info: object) {
    state.info = info
  },
  menuChange(state: State, menu: object) {
    state.menu = menu
  },
  orgChange(state: State, orgList: object) {
    state.orgList = orgList
  },
  authorityChange(state: State, authoritys: object) {
    state.authoritys = authoritys
  }
}

// actions
const actions = {
  // login by login.vue
  login({ commit, dispatch }: ActionContext<State, State>, params: any) {
    return new Promise((resolve, reject) => {
      loginApi(params).then((res:any) => {
        if(res.success){
          commit('tokenChange', res.msg.token)
          commit('infoChange', res.msg)
          resolve(res.msg)
          // dispatch('getInfo', { token: res.msg.token }).then(infoRes => {
          //   resolve(res.msg.token)
          // })
        }else{
          ElMessage.warning({message: res.err||'登陆失败！',type: 'warning'});
          reject(res)
        }
      })
    })
  },
  setmenu({commit}: ActionContext<State, State>,menu: any){
    commit('menuChange', menu)
  },
  setorg({commit}: ActionContext<State, State>,orglist: any){
    commit('orgChange', orglist)
  },
  setauthority({commit}: ActionContext<State, State>,authoritys: any){
    commit('authorityChange', authoritys)
  },
  // get user info after user logined
  getInfo({ commit }: ActionContext<State, State>, params: any) {
    return new Promise((resolve, reject) => {
      getInfoApi(params)
      .then(res => {
        commit('infoChange', res.data.info)
        resolve(res.data.info)
      })
    })
  },

  // login out the system after user click the loginOut button
  loginOut({ commit }: ActionContext<State, State>) {
    loginOutApi()
    .then(res => {

    })
    .catch(error => {

    })
    .finally(() => {
      localStorage.removeItem('tabs')
      localStorage.removeItem('vuex')
      location.reload()
    })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}