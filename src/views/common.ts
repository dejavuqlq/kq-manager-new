// 配置文件域名
export const fileHost = () => {
  return "http://kq-community.file.zhhost.top/"
};
// 配置文件域名-oss
export const ossHost = () => {
  return 'http://kq-community.file.zhhost.top/'
};
// 本地未上传地址
export const beforeUpload = () => {
  return "http://kq-community.server.zhhost.top/"
};
// 配置文件域名
export const uploadHost = () => {
  return "http://kq-community.server.zhhost.top/zh8848/upload"
};
// 图片临时回显地址
export const imagesDisplayHost = () => {
    return "http://kq-community.server.zhhost.top/upload/"
};
// 社工照片显示
export const usersrImg = () => {
  return "http://kq-community.file.zhhost.top/users/"
}
// 工单图片显示
export const jobsImg = () => {
  return "http://kq-community.file.zhhost.top/jobs/"
}
// banner图片显示
export const bannerImg = () => {
  return "http://kq-community.file.zhhost.top/banner/"
}
// 纠纷图片显示
export const disputeImg = () => {
  return "http://kq-community.file.zhhost.top/dispute/"
}
// 维修图片显示
export const propertyImg = () => {
  return "http://kq-community.file.zhhost.top/property/"
}
// 疫苗 医疗 服务预约图片显示
export const appointmentsImg = () => {
  return "http://kq-community.file.zhhost.top/appointments/"
}
// 活动图片显示
export const activityImg = () => {
  return "http://kq-community.file.zhhost.top/activity/"
}
// 党建图片显示
export const partyImg = () => {
  return "http://kq-community.file.zhhost.top/party/"
}


// 配置 身份
export const isworkersType = () => {
  return [
    {id: '0', title: '居民'},
    {id: '1', title: '社工'},
    {id: '2', title: '领导'},
  ]
}

// 配置 社工等级
export const leaves = () => {
  return {
    "0": [{id: '-1', title: '普通居民'}],//0-居民
    "1": [
      {id: '0', title: '工作人员'},
      {id: '1', title: '普通党员'},
      {id: '2', title: '主任'},
      {id: '3', title: '委员'},
      {id: '4', title: '副书记'},
      {id: '5', title: '书记'},
    ],//1-社工
    "2": [//2-领导
      {id: '1', title: '普通党员'},
      {id: '2', title: '主任'},
      {id: '3', title: '委员'},
      {id: '4', title: '副书记'},
      {id: '5', title: '书记'},
    ],
  }
}