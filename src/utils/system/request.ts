import axios , { AxiosError, AxiosRequestConfig, AxiosResponse, AxiosInstance } from 'axios'
import store from '@/store'
import { useStore } from 'vuex'
import { ElMessage } from 'element-plus'
const baseURL: any = import.meta.env.VITE_BASE_URL


const service: AxiosInstance = axios.create({
  baseURL: 'http://kq-community.server.zhhost.top/',
  timeout: 5000
})

// 请求前的统一处理
service.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // JWT鉴权处理
    if (store.getters['user/token']) {
      config.headers['token'] = store.state.user.token
    }
    return config
  },
  (error: AxiosError) => {
    // console.log(error) // for debug
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  (response: AxiosResponse) => {
    // console.log(response);
    
    const data = response.data
    
    if (response.status === 200) {
      if(!data.success&&data.err==='登录已过期，请重新登录'){
        ElMessage.warning({message: '登录已过期，请重新登录',type: 'warning'});
        store.dispatch('user/loginOut');
      }
      return data
    } else {
      showError(data)
      return Promise.reject(data)
    }
  },
  (error: AxiosError)=> {
    // console.log(error) // for debug
    const badMessage: any = error.message || error
    const code = parseInt(badMessage.toString().replace('Error: Request failed with status code ', ''))
    showError({ code, message: badMessage })
    return Promise.reject(error)
  }
)

function showError(error: any) {
  if (error.code === 403) {
    // to re-login
    store.dispatch('user/loginOut')
  } else {
    ElMessage({
      message: error.msg || error.message || '服务异常',
      type: 'error',
      duration: 3 * 1000
    })
  }
  
}

export default service