export default {
  menu: {
    dashboard: {
      name: 'dashboard',
      index: '首页'
    },
    system: {
      name: '系统目录',
      redirect: '重定向页面',
      '404': '404',
      '401': '401'
    },
    page: {
      name: '页面',
      crudTable: '业务表格',
      categoryTable: '分类联动表格',
      treeTable: '树联动表格'
    },
    systemmanage: {
      name: '系统设置',
      manageruser: '管理员管理',
      banner: '轮播图',
      organization: '组织架构'
    },
    usersmanage: {
      name: '人员管理',
      users: '用户管理',
      patrol: '巡逻记录',
      sign: '签到管理"',
      jobs: '工单管理"'
    },
    warningmanage: {
      name: '预警配置',
      warning_user: '预警社工配置',
      warning_water: '用水预警',
      warning_sensor: '烟感水压预警"',
      warning_health: '健康码预警"'
    },
    housemanage: {
      name: '房屋管理',
      house: '房屋管理'
    },
    carmanage: {
      name: '车辆管理',
      car: '车辆管理'
    },
    partybuildchmanage: {
      name: '党建管理',
      party_branch: '支部管理',
      party_apply: '申请管理',
      party_inspect: '联审管理"',
      party_news: '资讯管理"',
      party_group: '共建单位"'
    },
    securitymanage: {
      name: '在线治理',
      dispute: '纠纷调解'
    },
    propertymanage: {
      name: '智慧物管',
      property: '物业维修',
      vote: '投票管理'
    },
    antiepidemicmanage: {
      name: '智慧防疫',
      vaccine: '疫情防控',
      vaccination: '疫苗接种'
    },
    activitymanage: {
      name: '惠民活动',
      medical: '医疗预约',
      server: '服务预约',
      activity: '活动报名"',
      activity_news: '活动报道"'
    },
    filemanage: {
      name: '文件',
      file: '文件管理'
    },
  },
}