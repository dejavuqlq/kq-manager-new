export default {
  menu: {
    dashboard: {
      name: 'dashboard',
      index: 'index'
    },
    system: {
      name: 'system',
      redirect: 'redirect',
      '404': '404',
      '401': '401'
    },
    page: {
      name: 'page',
      crudTable: 'crudTable',
      categoryTable: 'categoryTable',
      treeTable: 'treeTable',
    },
    systemmanage: {
      name: 'systemmanage',
      manageruser: 'manageruser',
      banner: 'banner',
      organization: 'organization'
    },
    usersmanage: {
      name: 'usersmanage',
      users: 'users',
      patrol: 'patrol',
      sign: 'sign"',
      jobs: 'jobs"'
    },
    warningmanage: {
      name: 'warningmanage',
      warning_user: 'warning_user',
      warning_water: 'warning_water',
      warning_sensor: 'warning_sensor"',
      warning_health: 'warning_health"'
    },
    housemanage: {
      name: 'housemanage',
      house: 'house'
    },
    carmanage: {
      name: 'carmanage',
      car: 'car'
    },
    partybuildchmanage: {
      name: 'partybuildchmanage',
      party_branch: 'party_branch',
      party_apply: 'party_apply',
      party_inspect: 'party_inspect"',
      party_news: 'party_news"',
      party_group: 'party_group"'
    },
    securitymanage: {
      name: 'securitymanage',
      dispute: 'dispute'
    },
    propertymanage: {
      name: 'propertymanage',
      property: 'property',
      vote: 'vote'
    },
    antiepidemicmanage: {
      name: 'antiepidemicmanage',
      vaccine: 'vaccine',
      vaccination: 'vaccination'
    },
    activitymanage: {
      name: 'activitymanage',
      medical: 'medical',
      server: 'server',
      activity: 'activity"',
      activity_news: 'activity_news"'
    },
    filemanage: {
      name: 'filemanage',
      file: 'file'
    },
  },
}