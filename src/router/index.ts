/**
 * @description 所有人可使用的参数配置列表
 * @params hideMenu: 是否隐藏当前路由结点不在导航中展示
 * @params alwayShow: 只有一个子路由时是否总是展示菜单，默认false
 */
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import store from '@/store'
import i18n from '@/locale'
import NProgress from '@/utils/system/nprogress'
import { changeTitle } from '@/utils/system/title'

// 动态路由相关引入数据
import Layout from '@/layout/index.vue'
import MenuBox from '@/components/menu/index.vue'
import { createNameComponent } from './createNode'

// 引入modules
import Dashboard from './modules/dashboard'
import Pages from './modules/pages'
import System from './modules/system'

let modules: object[] = [
  ...System
]

const { t } = i18n.global

const routes: any = modules



const router = createRouter({
  history: createWebHashHistory(),
  routes
})
let asyncRoutes: RouteRecordRaw[] = [
  ...Dashboard,
  ...Pages,
]
// 动态路由的权限新增，供登录后调用
export function addRoutes() {
  // 与后端交互的逻辑处理，处理完后异步添加至页面
  asyncRoutes.forEach(item => {
    modules.push(item)
    router.addRoute(item)
  })

  let data = JSON.parse(JSON.stringify((store as any).state.user.menu))
  eachData(data, 0, '')
  data.forEach((item:any) => {
    modules.push(item)
    router.addRoute(item)
  })
}

//引入所有views下.vue文件 用于动态配置路由文件
const modules_:any = import.meta.glob("../views/main/**/**/index.vue");

// 重置匹配所有路由的解决方案，todo
function eachData(data: any, type: number, path:any) {
  data.forEach((d:any) => {
    if (d.children && d.children.length > 0) {
      if (type === 0) {
        d.component = Layout
      } else {
        d.component = createNameComponent(MenuBox)
      }
      eachData(d.children, type + 1,d.path)
    } else {
      /* 动态配置路由文件 */
      // console.log(path);
      
      d.component = createNameComponent(modules_[`../views/main/${path.split('/')[1]}/${d.component}/index.vue`])
    }
  })
}

if ((store as any).state.user.token) {
  addRoutes()
}

const whiteList = ['/login']

router.beforeEach((to, _from, next) => {
  NProgress.start();
  if ((store as any).state.user.token || whiteList.indexOf(to.path) !== -1) {
    to.meta.title ? (changeTitle(to.meta.title)) : ""; // 动态title
    next()
  } else {
    next("/login"); // 全部重定向到登录页
    to.meta.title ? (changeTitle(to.meta.title)) : ""; // 动态title
  }
});

router.afterEach((to, _from) => {
  const keepAliveComponentsName = store.getters['keepAlive/keepAliveComponentsName'] || []
  const name = to.matched[to.matched.length - 1].components.default.name
  if (to.meta && to.meta.cache && name && !keepAliveComponentsName.includes(name)) {
    store.commit('keepAlive/addKeepAliveComponentsName', name)
  }
  NProgress.done();
});

export {
  modules
}

export default router