import request from '@/utils/system/request'

// 获取管理员列表
export function getManagerList(data: object) {
  return request({
    url: '/manager/manageruser/list',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
// 获取管理员数据
export function getManageruser(data: object) {
  return request({
    url: '/manager/manageruser/get',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
//新增编辑管理员数据
export function designManager(data: object) {
  return request({
    url: '/manager/manageruser/design',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
//删除管理员
export function delManager(data: object) {
  return request({
    url: '/manager/manageruser/delete',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}


// 获取banner列表
export function getBannerList(data: object) {
  return request({
    url: '/manager/banner/list',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
//新增编辑banner数据
export function designBanner(data: object) {
  return request({
    url: '/manager/banner/design',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
// 删除banner
export function delBanner(data: object) {
  return request({
    url: '/manager/banner/delete',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}


//获取组织列表
export function getOrgList(data: object) {
  return request({
    url: '/manager/organization/list',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
//新增编辑组织数据
export function designOrg(data: object) {
  return request({
    url: '/manager/organization/design',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
// 删除组织
export function delOrg(data: object) {
  return request({
    url: '/manager/organization/delete',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}


// 获取用户列表
export function getUsersList(data: object) {
  return request({
    url: '/manager/users/list',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
// 获取用户数据
export function getUsers(data: object) {
  return request({
    url: '/manager/users/get',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
//新增编辑用户数据
export function designUsers(data: object) {
  return request({
    url: '/manager/users/design',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
//删除用户
export function delUsers(data: object) {
  return request({
    url: '/manager/users/delete',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
// 上传人脸
export function upUsersFace(data: object) {
  return request({
    url: '/manager/users/face',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
// 删除人脸
export function delUsersFace(data: object) {
  return request({
    url: '/manager/users/delface',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}





// 获取数据api
export function getData(data: object) {
  return request({
    url: '/table/list',
    method: 'post',
    baseURL: '/mock',
    data
  })
}

// 获取分类数据
export function getCategory(data: object) {
  return request({
    url: '/table/category',
    method: 'post',
    baseURL: '/mock',
    data
  })
}

// 获取树组织数据
export function getTree(data: object) {
  return request({
    url: '/table/tree',
    method: 'post',
    baseURL: '/mock',
    data
  })
}

// 新增
export function add(data: object) {
  return request({
    url: '/table/add',
    method: 'post',
    baseURL: '/mock',
    data
  })
}

// 编辑
export function update(data: object) {
  return request({
    url: '/table/update',
    method: 'post',
    baseURL: '/mock',
    data
  })
}

// 删除
export function del(data: object) {
  return request({
    url: '/table/del',
    method: 'post',
    baseURL: '/mock',
    data
  })
}