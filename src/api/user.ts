import request from '@/utils/system/request'

// 登录api
export function loginApi(data: object) {
  return request({
    url: '/manager/login',
    method: 'post',
    data
  })
}

// 获取用户信息Api
export function getInfoApi(data: object) {
  return request({
    url: '/user/info',
    method: 'post',
    baseURL: '/mock',
    data
  })
}

// 退出登录Api
export function loginOutApi() {
  return request({
    url: '/user/out',
    method: 'post',
    baseURL: '/mock'
  })
}

// 修改密码
export function passwordChange(data: object) {
  return request({
    url: '/manager/editpass',
    method: 'post',
    // baseURL: '/mock',
    data
  })
}
